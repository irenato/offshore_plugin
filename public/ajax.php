<?php


add_action('wp_ajax_nopriv_getSResults', 'getSResults');
add_action('wp_ajax_getSResults', 'getSResults');

function getSResults()
{
    $search_param = str_replace(' ', '%20', trim($_POST['search_param']));
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_USERPWD, get_option('ch_search_api_key'));
    curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/search/companies?q=' . $search_param . '&&items_per_page=99');
//    curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/search/companies?q=' . $search_param);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($curl);
    if ($errno = curl_errno($curl)) {
        $error_message = curl_strerror($errno);
        echo "cURL error ({$errno}):\n {$error_message}";
    }
    curl_close($curl);
    $response = json_decode($response);

    echo json_encode($response->items);
    die();
}

add_action('wp_ajax_nopriv_getCompanyBasicInformation', 'getCompanyBasicInformation');
add_action('wp_ajax_getCompanyBasicInformation', 'getCompanyBasicInformation');

function getCompanyBasicInformation()
{
    $id = trim($_POST['id']);
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_USERPWD, get_option('ch_search_api_key'));
    curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/company/' . $id);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($curl);
    if ($errno = curl_errno($curl)) {
        $error_message = curl_strerror($errno);
        echo "cURL error ({$errno}):\n {$error_message}";
    }
    curl_close($curl);

    echo $response;
    die();
}

add_action('wp_ajax_nopriv_getCompanyBasicHistory', 'getCompanyBasicHistory');
add_action('wp_ajax_getCompanyBasicHistory', 'getCompanyBasicHistory');

function getCompanyBasicHistory()
{
    $id = trim($_POST['id']);
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_USERPWD, get_option('ch_search_api_key'));
    curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/company/' . $id . '/filing-history');
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($curl);
    if ($errno = curl_errno($curl)) {
        $error_message = curl_strerror($errno);
        echo "cURL error ({$errno}):\n {$error_message}";
    }
    curl_close($curl);

    echo $response;
    die();
}

add_action('wp_ajax_nopriv_getCompanyBasicPeople', 'getCompanyBasicPeople');
add_action('wp_ajax_getCompanyBasicPeople', 'getCompanyBasicPeople');

function getCompanyBasicPeople()
{
    $id = trim($_POST['id']);
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_USERPWD, get_option('ch_search_api_key'));
    curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/company/' . $id . '/officers');
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($curl);
    if ($errno = curl_errno($curl)) {
        $error_message = curl_strerror($errno);
        echo "cURL error ({$errno}):\n {$error_message}";
    }
    curl_close($curl);

    echo $response;
    die();
}

add_action('wp_ajax_nopriv_getCompanyBasicPeopleSC', 'getCompanyBasicPeopleSC');
add_action('wp_ajax_getCompanyBasicPeopleSC', 'getCompanyBasicPeopleSC');

function getCompanyBasicPeopleSC()
{
    $id = trim($_POST['id']);
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_USERPWD, get_option('ch_search_api_key'));
    curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/company/' . $id . '/persons-with-significant-control');
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($curl);
    if ($errno = curl_errno($curl)) {
        $error_message = curl_strerror($errno);
        echo "cURL error ({$errno}):\n {$error_message}";
    }
    curl_close($curl);

    echo $response;
    die();
}