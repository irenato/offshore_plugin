var $ = jQuery;
jQuery(document).ready(function ($) {
    var preloader_area = "<div id='page-preloader' style='display: none'><span class='spinner'></span></div>",
        body = document.getElementsByTagName('body');
    body[0].insertAdjacentHTML('afterbegin', preloader_area);
})

function mainSearchFunction() {
    var search_param = ($('#ch-search-input').val()).length > 0 ? $('#ch-search-input').val() : localStorage.getItem('searchValue'),
        search_request = $.ajax({
            url: '/blog/wp-admin/admin-ajax.php',
            type: 'POST',
            beforeSend: function () {
                $('#page-preloader').show();
            },
            data: {
                'action': 'getSResults',
                'search_param': search_param,
            },
            success: getResList,
        });

    function getResList(result) {
        $('#page-preloader').hide();
        result = JSON.parse(result || null);
        var str = '',
            str2 = '<h2>Не точное совпадение</h2>';
        if (result !== null) {
            $('#not-items-found').detach();
            str += '<div class="search-results-list">';
            str2 += '<ul>';
            for (i = 0; i < result.length; i++) {
                var all_data = JSON.stringify(result[i]);
                if (i == 0) {
                    str += '<h2>Точное совпадение</h2>';
                    str += '<ul>';
                    str += "<li data-object='" + all_data + "'><a href='#' data-id=" + result[i].company_number + " class='search-result-item'>" + result[i].title + "</a></li>";
                    str += '</ul>';
                }
                else {
                    str2 += "<li data-object='" + all_data + "'><a href='#' data-id=" + result[i].company_number + " class='search-result-item'>" + result[i].title + "</a></li>";
                }
            }
            str2 += '</ul>';
            str += str2;
            str += '</div>';
            $('#search-app').empty();
            $('#search-app').append(form_el);
            $('#ch-search-input').val('');
            $('#search-app').append(str);
            localStorage.setItem('searchValue', search_param);
            return false;
        } else {
            $('#ch-search-input').val('');
            $('#not-items-found').detach();
            str = '<h2 id="not-items-found">Нет соответствий по Вашему запросу</h2>';
            $('#search-app').append(form_el);
            $('#search-app').append(str);
        }
    }
}

var form_el = $('#ch-form-field');

new Vue({
    el: '#search-app',

    methods: {
        searchFunction: mainSearchFunction,
    }
});

$(document).on('click', '#ch-search-button', function(e){
    e.preventDefault();
    mainSearchFunction();
})

var menu = '',
    comp_n = '',
    c_n = '';

$('body').on('click', 'a.search-result-item', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id'),
        get_company_information = $.ajax({
            url: '/blog/wp-admin/admin-ajax.php',
            type: 'POST',
            beforeSend: function () {
                $('#page-preloader').show();
            },
            data: {
                'action': 'getCompanyBasicInformation',
                'id': id,
            },
            success: getBasicInformation,
        });

    function getBasicInformation(result) {
        $('#page-preloader').hide();
        result = JSON.parse(result || null);
        var block = '';
        comp_n = result.company_name;
        c_n = result.company_number;
        menu += "<h2 class='company-title title'>" + comp_n + "</h2>";
        menu += "<h4 class='title'> Номер компании: " + c_n + "</h4>";
        menu += "<ul class='nav nav-tabs'>";
        menu += "<li class='active'><a href='#block' class='search-result-item' data-toggle='tab' data-id='" + id + "'>Обзор</a></li>";
        menu += "<li><a href='#block-2' class='search-history-item'  data-toggle='tab' data-id='" + id + "'>Документы</a></li>";
        menu += "<li><a href='#block-3' class='search-people-item' data-toggle='tab' data-id='" + id + "'>Люди</a></li>";
        menu += "</ul>";
        block += "<div class='tab-content'>";
        block += "<div class='tab-pane active' id='block'>";
        if (result.registered_office_address != null) {
            if (result.registered_office_address.address_line_1 !== null && result.registered_office_address.locality !== null && result.registered_office_address.region !== null && result.registered_office_address.postal_code !== null) {
                block += "<h4 class='title'>Юридический адрес</h4>";
                block += "<p class='title'>" + (!!result.registered_office_address.address_line_1 ? result.registered_office_address.address_line_1 + ', ' : '') + (!!result.registered_office_address.locality ? result.registered_office_address.locality + ', ' : '') + (!!result.registered_office_address.region ? result.registered_office_address.region + ', ' : '') + (!!result.registered_office_address.postal_code ? result.registered_office_address.postal_code : '') + "</p>";
            }
        }
        if (!!result.company_status) {
            block += "<h4 class='title'>Статус компании</h4>";
            block += "<p class='company-title title'>" + result.company_status + (!!result.company_status_detail ? ' - ' + result.company_status_detail.replace(/-/g, " ") : '') + "</p>";
        }
        if (result.status != null) {
            block += "<h4 class='title'>Статус компании</h4>";
            block += "<p class='title'>" + result.status + "</p>";
        }
        if (result.type != null) {
            block += "<h4 class='title'>Тип компании</h4>";
            block += "<p class='title'>" + (result.type).replace(/-/g, " ") + "</p>";
        }
        if (!!result.date_of_creation) {
            block += "<h4 class='title'>Дата основания</h4>";

            block += "<p class='title'>" + formatingDate(result.date_of_creation) + "</p>";
        }
        if (result.accounts) {
            if (result.accounts.next_made_up_to != null) {
                if (result.accounts.next_made_up_to !== null && result.accounts.next_due !== null) {
                    block += "<h4 class='title'>Отчеты";
                    if (checkDate(result.accounts.next_due))
                        block += "<span class='overdue'>ПРОСРОЧЕННЫЕ</span></h4>";
                    else
                        block += "</h4>";
                    block += "<p class='title'>Следующий отчетный период с <strong>" + formatingDate(result.accounts.next_made_up_to) + "</strong> по <strong>" + formatingDate(result.accounts.next_due) + "</strong></p>";
                }
            }
            if (result.accounts.last_accounts.type != null)
                if (!!result.accounts.last_accounts.made_up_to)
                    block += "<p class='title'>Последняя дата сдачи финансового отчета <strong>" + formatingDate(result.accounts.last_accounts.made_up_to) + "</strong></p>";
        }
        if (result.confirmation_statement) {
            if (result.confirmation_statement.next_made_up_to !== null && result.confirmation_statement.next_due !== null) {
                block += "<h4 class='title'>Ведомость";
                if (checkDate(result.confirmation_statement.next_due))
                    block += "<span class='overdue'>ПРОСРОЧЕННАЯ</span></h4>";
                else
                    block += "</h4>";
                block += "<p class='title'>Следующая дата сдачи ведомости с <strong>" + formatingDate(result.confirmation_statement.next_made_up_to) + "</strong> по <strong>" + formatingDate(result.confirmation_statement.next_due) + "</strong></p>";
                if (!!result.confirmation_statement.last_made_up_to)
                    block += "<p class='title'>Последняя дата сдачи ведомости <strong>" + formatingDate(result.confirmation_statement.last_made_up_to) + "</strong></p>";
            }
        }
        if (result.annual_return) {
            if (!!result.annual_return.last_made_up_to) {
                block += "<h4 class='title'>Декларация";
                if (checkDate(result.annual_return.last_made_up_to) && !!result.annual_return.next_due)
                    block += "<span class='overdue'>ПРОСРОЧЕННАЯ</span></h4>";
                else
                    block += "</h4>";
                if (!!result.annual_return.next_due && !!result.annual_return.last_made_up_to) {
                    block += "<p class='title'>Следующее декларирование с <strong>" + formatingDate(result.annual_return.last_made_up_to) + "</strong> по <strong>" + formatingDate(result.annual_return.next_due) + "</strong></p>";
                }
                block += "<p class='title'>Последняя декларация была составлена <strong>" + formatingDate(result.annual_return.last_made_up_to) + "</strong></p>";
            }
        }
        if (result.sic_codes) {
            block += "<h4 class='title'>Вид деятельности (SIC)</h4>";
            block += "<p class='title'>" + result.sic_codes[0] + "</p>";
        }
        block += '</div>';
        block += '</div>';
        block = menu + block;
        menu = '';
        $('#search-app').empty();
        $('#search-app').append(form_el);
        $('#ch-search-input').val('');
        $('#search-app').append(block);
        $('#search-app').append("<a href='#' id='back-to-list' class='button'>Вернуться обратно</a>");
        return false;
    }
})


$('body').on('click', 'a.search-history-item', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id'),
            get_company_information = $.ajax({
                url: '/blog/wp-admin/admin-ajax.php',
                type: 'POST',
                beforeSend: function () {
                    $('#page-preloader').show();
                },
                data: {
                    'action': 'getCompanyBasicHistory',
                    'id': id,
                },
                success: getBasicHistory,
            });

        function getBasicHistory(result) {
            $('#page-preloader').hide();
            result = JSON.parse(result || null);
            if (result != null) {
                var block = '',
                    items = result.items;
                menu += "<h2 class='company-title title'>" + comp_n + "</h2>";
                menu += "<h4 class='title'> Номер компании: " + c_n + "</h4>";
                menu += "<ul class='nav nav-tabs'>";
                menu += "<li><a href='#block' class='search-result-item' data-toggle='tab' data-id='" + id + "'>Обзор</a></li>";
                menu += "<li class='active'><a href='#block-2' class='search-history-item'  data-toggle='tab' data-id='" + id + "'>Документы</a></li>";
                menu += "<li><a href='#block-3' class='search-people-item' data-toggle='tab' data-id='" + id + "'>Люди</a></li>";
                menu += "</ul>";
                block += "<div class='tab-content'>";
                block += "<div class='tab-pane active' id='block-2'>";
                block += "<div class='tr'>";
                block += "<div class='th'>";
                block += "<p><strong>Дата</strong></p>";
                block += "</div>";
                block += "<div class='th'>";
                block += "<p><strong>Описание</strong></p>";
                block += "</div>";
                block += "<div class='th'>";
                block += "<p><strong>Просмотреть</strong></p>";
                block += "</div>";
                block += "</div>";
                for (i = 0; i < items.length; i++) {
                    if (!!items[i].links) {
                        block += "<div class='tr'>";
                        block += "<div class='td'>";
                        block += "<p>" + formatingDate(items[i].date) + "</p>";
                        block += "</div>";
                        block += "<div class='td'>";
                        if (!!items[i].description_values) {
                            if (!!items[i].description_values.made_up_date)
                                var desc_details = ' made up to ' + formatingDate(items[i].description_values.made_up_date);
                            else if (!!items[i].description_values.change_date && !!items[i].description_values.officer_name)
                                var desc_details = ' for ' + items[i].description_values.officer_name + ' on ' + formatingDate(items[i].description_values.change_date);
                            else if (!!items[i].description_values.officer_name)
                                var desc_details = ' of ' + items[i].description_values.officer_name + ' as a member ';
                            if (!!items[i].description_values.description)
                                block += "<p><span class='document-description'>" + items[i].description_values.description + "</span>" + desc_details + "</p>";
                            else
                                block += "<p><span class='document-description'>" + items[i].description.replace(/-/g, " ") + "</span>" + desc_details + "</p>";
                        } else
                            block += "<p><span class='document-description'>" + items[i].description.replace(/-/g, " ") + "</span></p>";
                        block += "</div>";
                        block += "<div class='td'>";
                        block += "<p class='download-pdf-now'><a href='https://beta.companieshouse.gov.uk" + items[i].links.self + "/document?format=pdf&download=0' target='_blank' download>открыть PDF</a> (" + items[i].pages + " pages)</p>";
                        block += "</div>";
                        block += "</div>";
                    }
                }
                block += "</div>";
                block = menu + block;
                menu = '';
                $('#search-app').empty();
                $('#search-app').append(form_el);
                $('#ch-search-input').val('');
                $('#search-app').append(block);
                $('#search-app').append("<a href='#' id='back-to-list' class='button'>Вернуться обратно</a>");
                return false;
            }
            else {
                var str = '<h2 id="not-items-found">Отсутствует информация по Вашему запросу</h2>';
                $('#search-app').append(form_el);
                $('#ch-search-input').val('');
                $('#search-app').append(str);
                $('#search-app').append("<a href='#' id='back-to-list' class='button'>Вернуться обратно</a>");
            }
        }

    }
)

$('body').on('click', 'a.search-people-item, #ch-search-people', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id'),
        get_company_information = $.ajax({
            url: '/blog/wp-admin/admin-ajax.php',
            type: 'POST',
            beforeSend: function () {
                $('#page-preloader').show();
            },
            data: {
                'action': 'getCompanyBasicPeople',
                'id': id,
            },
            success: getPeople,
        });

    function getPeople(result) {
        $('#page-preloader').hide();
        result = JSON.parse(result || null);
        if (result != null || result.items.length == 0) {
            var block = '',
                items = result.items;
            menu += "<h2 class='company-title title'>" + comp_n + "</h2>";
            menu += "<h4 class='title'> Номер компании: " + c_n + "</h4>";
            menu += "<ul class='nav nav-tabs'>";
            menu += "<li><a href='#block' class='search-result-item' data-toggle='tab' data-id='" + id + "'>Обзор</a></li>";
            menu += "<li><a href='#block-2' class='search-history-item'  data-toggle='tab' data-id='" + id + "'>Документы</a></li>";
            menu += "<li  class='active'><a href='#block-3' class='search-people-item' data-toggle='tab' data-id='" + id + "'>Люди</a></li>";
            menu += "</ul>";
            block += "<div class='tab-content'>";
            block += "<div class='tab-pane active' id='block-3'>";
            block += "<div class='officers'>";
            block += "<ul class='buttons'>";
            block += "<li class='active'><a id='ch-search-people' href='#' data-id='" + id + "'>Люди</a></li>";
            block += "<li><a id='ch-search-people-sc' href='#' data-id='" + id + "'>Лица со значительным контролем</a></li>";
            block += "</ul>";
            block += "<h3 class='title'>Люди:&nbsp </h3>";
            if (!!result.total_results)
                block += "<h3 class='title'> " + result.total_results + "  найдено</h3>";
            else
                block += "<h3 class='title'>информация не найдена</h3>";
            if (items) {
                for (i = 0; i < items.length; i++) {
                    block += "<div class='officer'>";
                    block += "<h2 class='title'>" + items[i].name;
                    if (!!!items[i].resigned_on)
                        block += "<span class='status active'>ACTIVE</span></h2>";
                    else
                        block += "<span class='status resigned'>RESIGNED</span></h2>";
                    block += "<h4 class='title'>Почтовый адрес</h4>";
                    if (!!items[i].address)
                        block += "<p class='title'>" + (!!items[i].address.premises ? items[i].address.premises + ', ' : '') + (!!items[i].address.address_line_1 ? items[i].address.address_line_1 + ', ' : '') + (!!items[i].address.address_line_2 ? items[i].address.address_line_2 + ', ' : '') + (!!items[i].address.locality ? items[i].address.locality + ', ' : '') + (!!items[i].address.region ? items[i].address.region + ', ' : '') + (!!items[i].address.country ? items[i].address.country + ', ' : '') + (!!items[i].address.postal_code ? items[i].address.postal_code : '') + "</p>";
                    block += "<ul class='description'>";
                    if (items[i].officer_role != null) {
                        block += "<li>";
                        block += "<h4 class='title'>Должность</h4>";
                        block += "<p class='title'>" + items[i].officer_role.replace(/-/g, " ") + "</p>";
                        block += "</li>";
                    }
                    if (!!items[i].notified_on) {
                        block += "<li>";
                        block += "<h4 class='title'>Зарегистрирован</h4>";
                        block += "<p class='title'>" + formatingDate(items[i].notified_on) + "</p>";
                        block += "</li>";
                    }
                    if (items[i].date_of_birth != null) {
                        block += "<li>";
                        block += "<h4 class='title'>Дата рождения</h4>";
                        block += "<p class='title'>" + items[i].date_of_birth.month + ' ' + items[i].date_of_birth.year + "</p>";
                        block += "</li>";
                    }
                    if (items[i].appointed_on != null) {
                        block += "<li>";
                        block += "<h4 class='title'>Назначен</h4>";
                        block += "<p class='title'>" + formatingDate(items[i].appointed_on) + "</p>";
                        block += "</li>";
                    }
                    if (!!items[i].resigned_on) {
                        block += "<li>";
                        block += "<h4 class='title'>Подал в отставку</h4>";
                        block += "<p class='title'>" + formatingDate(items[i].resigned_on) + "</p>";
                        block += "</li>";
                    }
                    if (!!items[i].identification) {
                        if (!!items[i].identification.identification_type) {
                            if (items[i].identification.identification_type === 'non-eea') {
                                block += "<h5 class='title'>Зарегистрирован не в Европейской экономической зоне</h5>";
                                block += "<br>";
                            }
                        }
                        if (!!items[i].identification.legal_authority) {
                            block += "<li>";
                            block += "<h4 class='title'>Регулируемый правовыми нормами</h4>";
                            block += "<p class='title'>" + items[i].identification.legal_authority + "</p>";
                            block += "</li>";
                        }
                        if (!!items[i].identification.legal_form) {
                            block += "<li>";
                            block += "<h4 class='title'>Юридическая форма</h4>";
                            block += "<p class='title'>" + items[i].identification.legal_form + "</p>";
                            block += "</li>";
                        }
                        if (!!items[i].identification.place_registered) {
                            block += "<li>";
                            block += "<h4 class='title'>Место регистрации</h4>";
                            block += "<p class='title'>" + items[i].identification.place_registered + "</p>";
                            block += "</li>";
                        }
                        if (!!items[i].identification.registration_number) {
                            block += "<li>";
                            block += "<h4 class='title'>Регистрационный номер</h4>";
                            block += "<p class='title'>" + items[i].identification.registration_number + "</p>";
                            block += "</li>";
                        }
                    }
                    if (items[i].nationality != null) {
                        block += "<li>";
                        block += "<h4 class='title'>Национальность</h4>";
                        block += "<p class='title'>" + items[i].nationality + "</p>";
                        block += "</li>";
                    }
                    if (items[i].country_of_residence != null) {
                        block += "<li>";
                        block += "<h4 class='title'>Страна проживания</h4>";
                        block += "<p class='title'>" + items[i].country_of_residence + "</p>";
                        block += "</li>"
                    }
                    if (items[i].occupation != null) {
                        block += "<li>";
                        block += "<h4 class='title'>Профессия</h4>";
                        block += "<p class='title'>" + items[i].occupation + "</p>";
                        block += "</li>"
                    }
                    if (!!items[i].natures_of_control) {
                        block += "<h4 class='title'>Характер управления</h4>";
                        for (z = 0; z < items[i].natures_of_control.length; z++) {
                            var new_str = items[i].natures_of_control[z].replace(/-/g, " ");
                            block += "<p class='people-nc title'>" + new_str + "</p>";
                        }
                    }
                    block += "</ul>";
                    block += "</div>";
                }
            }
            block += "</div>";
            block += "</div>";
            block += "</div>";
            block = menu + block;
            menu = '';
            $('#search-app').empty();
            $('#search-app').append(form_el);
            $('#ch-search-input').val('');
            $('#search-app').append(block);
            $('#search-app').append("<a href='#' id='back-to-list' class='button'>Вернуться обратно</a>");
            return false;
        } else {
            var str = '<h2 id="not-items-found">Отсутствует информация по Вашему запросу</h2>';
            $('#search-app').append(form_el);
            $('#ch-search-input').val('');
            $('#search-app').append(str);
            $('#search-app').append("<a href='#' id='back-to-list' class='button'>Вернуться обратно</a>");
        }
    }

})

$('body').on('click', '#ch-search-people-sc', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id'),
        get_company_information = $.ajax({
            url: '/blog/wp-admin/admin-ajax.php',
            type: 'POST',
            beforeSend: function () {
                $('#page-preloader').show();
            },
            data: {
                'action': 'getCompanyBasicPeopleSC',
                'id': id,
            },
            success: getPeople,
        });

    function getPeople(result) {
        $('#page-preloader').hide();
        result = JSON.parse(result || null);
        if (result != null || result.items.length == 0) {
            var block = '',
                items = result.items;
            menu += "<h2 class='company-title title'>" + comp_n + "</h2>";
            menu += "<h4 class='title'> Номер компании: " + c_n + "</h4>";
            menu += "<ul class='nav nav-tabs'>";
            menu += "<li><a href='#block' class='search-result-item' data-toggle='tab' data-id='" + id + "'>Обзор</a></li>";
            menu += "<li><a href='#block-2' class='search-history-item'  data-toggle='tab' data-id='" + id + "'>Документы</a></li>";
            menu += "<li  class='active'><a href='#block-3' class='search-people-item' data-toggle='tab' data-id='" + id + "'>Люди</a></li>";
            menu += "</ul>";
            block += "<div class='tab-content'>";
            block += "<div class='tab-pane active' id='block-3'>";
            block += "<div class='officers'>";
            block += "<ul class='buttons'>";
            block += "<li><a id='ch-search-people' href='#' data-id='" + id + "'>Люди</a></li>";
            block += "<li class='active'><a id='ch-search-people-sc' href='#' data-id='" + id + "'>Лица со значительным контролем</a></li>";
            block += "</ul>";
            block += "<h3 class='title'>Люди:&nbsp </h3>";
            if (!!result.total_results)
                block += "<h3 class='title'> " + result.total_results + "  найдено</h3>";
            else
                block += "<h3 class='title'> информация не найдена</h3>";
            if (items) {
                for (i = 0; i < items.length; i++) {
                    block += "<div class='officer'>";
                    block += "<h2 class='title'>" + items[i].name;
                    if (!!!items[i].resigned_on)
                        block += "<span class='status active'>ACTIVE</span></h2>";
                    else
                        block += "<span class='status resigned'>RESIGNED</span></h2>";
                    block += "<h4 class='title'>Почтовый адрес</h4>";
                    if (!!items[i].address)
                        block += "<p class='title'>" + (!!items[i].address.premises ? items[i].address.premises + ', ' : '') + (!!items[i].address.address_line_1 ? items[i].address.address_line_1 + ', ' : '') + (!!items[i].address.address_line_2 ? items[i].address.address_line_2 + ', ' : '') + (!!items[i].address.locality ? items[i].address.locality + ', ' : '') + (!!items[i].address.region ? items[i].address.region + ', ' : '') + (!!items[i].address.country ? items[i].address.country + ', ' : '') + (!!items[i].address.postal_code ? items[i].address.postal_code : '') + "</p>";
                    block += "<ul class='description'>";
                    if (items[i].officer_role != null) {
                        block += "<li>";
                        block += "<h4 class='title'>Должность</h4>";
                        block += "<p class='title'>" + items[i].officer_role.replace(/-/g, " ") + "</p>";
                        block += "</li>";
                    }
                    if (!!items[i].notified_on) {
                        block += "<li>";
                        block += "<h4 class='title'>Зарегистрирован</h4>";
                        block += "<p class='title'>" + formatingDate(items[i].notified_on) + "</p>";
                        block += "</li>";
                    }
                    if (items[i].date_of_birth != null) {
                        block += "<li>";
                        block += "<h4 class='title'>Дата рождения</h4>";
                        block += "<p class='title'>" + items[i].date_of_birth.month + ' ' + items[i].date_of_birth.year + "</p>";
                        block += "</li>";
                    }
                    if (items[i].appointed_on != null) {
                        block += "<li>";
                        block += "<h4 class='title'>Назначен</h4>";
                        block += "<p class='title'>" + formatingDate(items[i].appointed_on) + "</p>";
                        block += "</li>";
                    }
                    if (!!items[i].resigned_on) {
                        block += "<li>";
                        block += "<h4 class='title'>Подал в отставку</h4>";
                        block += "<p class='title'>" + formatingDate(items[i].resigned_on) + "</p>";
                        block += "</li>";
                    }
                    if (!!items[i].identification) {
                        if (!!items[i].identification.identification_type) {
                            if (items[i].identification.identification_type === 'non-eea') {
                                block += "<h5 class='title'>Зарегистрирован не в Европейской экономической зоне</h5>";
                                block += "<br>";
                            }
                        }
                        if (!!items[i].identification.legal_authority) {
                            block += "<li>";
                            block += "<h4 class='title'>Регулируемый правовыми нормами</h4>";
                            block += "<p class='title'>" + items[i].identification.legal_authority + "</p>";
                            block += "</li>";
                        }
                        if (!!items[i].identification.legal_form) {
                            block += "<li>";
                            block += "<h4 class='title'>Юридическая форма</h4>";
                            block += "<p class='title'>" + items[i].identification.legal_form + "</p>";
                            block += "</li>";
                        }
                        if (!!items[i].identification.place_registered) {
                            block += "<li>";
                            block += "<h4 class='title'>Место регистрации</h4>";
                            block += "<p class='title'>" + items[i].identification.place_registered + "</p>";
                            block += "</li>";
                        }
                        if (!!items[i].identification.registration_number) {
                            block += "<li>";
                            block += "<h4 class='title'>Регистрационный номер</h4>";
                            block += "<p class='title'>" + items[i].identification.registration_number + "</p>";
                            block += "</li>";
                        }
                    }
                    if (items[i].nationality != null) {
                        block += "<li>";
                        block += "<h4 class='title'>Национальность</h4>";
                        block += "<p class='title'>" + items[i].nationality + "</p>";
                        block += "</li>";
                    }
                    if (items[i].country_of_residence != null) {
                        block += "<li>";
                        block += "<h4 class='title'>Страна проживания</h4>";
                        block += "<p class='title'>" + items[i].country_of_residence + "</p>";
                        block += "</li>"
                    }
                    if (items[i].occupation != null) {
                        block += "<li>";
                        block += "<h4 class='title'>Профессия</h4>";
                        block += "<p class='title'>" + items[i].occupation + "</p>";
                        block += "</li>"
                    }
                    if (!!items[i].natures_of_control) {
                        block += "<h4 class='title'>Характер управления</h4>";
                        for (z = 0; z < items[i].natures_of_control.length; z++) {
                            var new_str = items[i].natures_of_control[z].replace(/-/g, " ");
                            block += "<p class='people-nc title'>" + new_str + "</p>";
                        }
                    }
                    block += "</ul>";
                    block += "</div>";
                }
            }
            block += "</div>";
            block += "</div>";
            block += "</div>";
            block = menu + block;
            menu = '';
            $('#search-app').empty();
            $('#search-app').append(form_el);
            $('#ch-search-input').val('');
            $('#search-app').append(block);
            $('#search-app').append("<a href='#' id='back-to-list' class='button'>Вернуться обратно</a>");
            return false;
        } else {
            var str = '<h2 id="not-items-found">Отсутствует информация по Вашему запросу</h2>';
            $('#search-app').append(form_el);
            $('#ch-search-input').val('');
            $('#search-app').append(str);
            $('#search-app').append("<a href='#' id='back-to-list' class='button'>Вернуться обратно</a>");
        }
    }

})

$('body').on('click', '#back-to-list', function (e) {
    e.preventDefault();
    mainSearchFunction();
})

function formatingDate(some_date) {
    if (!!some_date) {
        var formatted_date = some_date.split('-');
        return formatted_date[2] + '.' + formatted_date[1] + '.' + formatted_date[0];
    }
}

function checkDate(some_date) {
    var date = Date.now(),
        formatted_date = some_date.split('-'),
        checked_date = new Date(formatted_date[0], formatted_date[1], formatted_date[2]);
    if (checked_date > date)
        return false;
    return true;
}

